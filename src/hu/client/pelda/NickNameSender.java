package hu.client.pelda;

import java.io.OutputStream;
import java.io.PrintWriter;

public class NickNameSender {

	private String nickname;
	private OutputStream os;
	
	public NickNameSender(String nickname, OutputStream os) { //TODO: jobb nevet kitalálni
		super();
		this.nickname = nickname;
		this.os = os;
	}
	
	public NickNameSender(String nickname) { //TODO: jobb nevet kitalálni
		super();
		this.nickname = nickname;
	}
	
	public void send() {
		PrintWriter pw = new PrintWriter(this.os, true);
		pw.println(nickname);
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public OutputStream getOs() {
		return os;
	}

	public void setOs(OutputStream os) {
		this.os = os;
	}
	
}
