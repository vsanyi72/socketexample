package hu.client.pelda;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class SenderThread extends Thread {
	
	private Scanner sc;
	private Socket socket;
	
	public SenderThread(Socket socket) {
		super();
		this.sc = new Scanner(System.in);
		this.socket = socket;
	}
	
	public SenderThread() {
		this.sc = new Scanner(System.in);
	}

	@Override
	public void run() {

		while (true) { // vagy meghatározni, hogy mire lép ki (/quit)
			try {
				// beolvas
				String input = sc.nextLine();
				// küld
				PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);
				pw.println(input);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public Scanner getSc() {
		return sc;
	}

	public void setSc(Scanner sc) {
		this.sc = sc;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
}
