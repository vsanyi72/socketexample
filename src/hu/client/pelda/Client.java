package hu.client.pelda;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

//TODO: Vizsgálni a szervertől visszakapott választ, nickname validálás végett
public class Client {

	private String host;
	private int port;
	private String nickname;
	private Socket socket;
	private NickNameSender nickNameSender;
	private SenderThread sender;
	private ReceiverThread receiver;
	
	public Client(String host, int port, String nickname) {
		this.host = host;
		this.port = port;
		this.nickname = nickname;
		this.nickNameSender = new NickNameSender(nickname);
		this.sender = new SenderThread();
		this.receiver = new ReceiverThread();
	}

	public boolean connect() throws UnknownHostException, IOException {
		this.socket = new Socket(this.host, this.port);
		this.nickNameSender.setOs(this.socket.getOutputStream());	
		this.nickNameSender.send();
		this.sender.setSocket(socket);
		this.receiver.setSocket(socket);
		
		return true;
	}
	
	public void startCommunication() {
		this.sender.start();
		this.receiver.start();
	}
	
	
}
