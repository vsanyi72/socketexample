package hu.client.pelda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ReceiverThread extends Thread {

	private Socket socket;
	
	public ReceiverThread(Socket socket) {
		super();
		this.socket = socket;
	}
	
	public ReceiverThread() {
		super();
	}

	@Override
	public void run() {

		while (true) {
			try {
				String sor;
				BufferedReader br = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
				while ((sor = br.readLine()) != null) {
					System.out.println(sor);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
}
